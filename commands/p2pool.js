const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const axios = require('axios'); 

module.exports = {
	data: new SlashCommandBuilder()
		.setName('p2pool')
		.setDescription('stats for P2Pool')
    .addStringOption(pool =>
      pool.setName("pool")
        .setDescription("Which pool to check")
        .setRequired(true)
        .addChoices(
          { name: "main", value: "main" },
          { name: "mini", value: "mini" },
        )),
	async execute(interaction) {
    const pool = interaction.options.getString("pool");
    var api_pool_info;

    if(pool == "main") {
      api_pool_info = "https://p2pool.observer/api/pool_info";
    } else {
      api_pool_info = "https://mini.p2pool.observer/api/pool_info";
    }
    
    axios.get(api_pool_info)
      .then(response => {
        const monero_height = response.data.sidechain.last_block.main_height;
        const p2pool_height = response.data.sidechain.last_block.side_height;
        const last_found_timestamp = response.data.sidechain.last_found.main_block.timestamp;
        const current_effort = +response.data.sidechain.effort.current.toFixed(2);
        const all_miners = response.data.sidechain.miners;
        const window_miners = response.data.sidechain.window.miners;
        const p2pool_version = response.data.versions.p2pool.version;
        const network_hashrate = (response.data.mainchain.difficulty / 150e9).toFixed(2);
        const hashrate = (response.data.sidechain.difficulty / 120e6).toFixed(2);

        const p2poolEmbed = new EmbedBuilder()
          .setColor("#ff6600")
          .setTitle(`P2Pool ${pool} stats`)
          .setDescription(`Data provided by https://p2pool.observer`)
          .setThumbnail("https://web.getmonero.org/press-kit/symbols/monero-symbol-480.png")
          .addFields(
            { name: ":bricks: Monero Height", value: `${monero_height}`, inline: true },
            { name: ":bricks: P2Pool Height", value: `${p2pool_height}`, inline: true },
            { name: ":stopwatch: Last Block was", value: `<t:${last_found_timestamp}:R>`, inline: true },
            { name: ":chart_with_upwards_trend: Monero Hashrate", value: `${network_hashrate} GH/s`, inline: true },
            { name: ":chart_with_upwards_trend: P2Pool Hashrate", value: `${hashrate} MH/s`, inline: true },
            { name: ":sweat: Current Effort", value: `${current_effort}%`, inline: true },
            { name: ":pick: Window Miners", value: `${window_miners}`, inline: true },
            { name: ":pick: Known Miners", value: `${all_miners}`, inline: true },
            { name: ":person_swimming: P2Pool Version", value: `${p2pool_version}`, inline: true })
          .setTimestamp()

        interaction.reply({ embeds:[p2poolEmbed] })
      }) .catch(error => {
        interaction.reply(`Error: ${error}`)
      })
	},
};

