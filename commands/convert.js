const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const cryptocompare = require('cryptocompare');
const dotenv = require('dotenv');

dotenv.config();

module.exports = {
	data: new SlashCommandBuilder()
		.setName('convert')
		.setDescription('Convert XMR to traditional currencies')
    .addNumberOption(amount =>
      amount.setName("amount")
        .setDescription("How much XMR to convert to traditional currencies")
        .setRequired(true)
    ),
	async execute(interaction) {
    const amount = interaction.options.getNumber("amount");
    const currencies = ["USD", "EUR", "CAD", "GBP", "AUD", "CHF", "JPY", "CNY", "INR"];
    const converted = [];

    try {
      for (const currency of currencies) {
        const data = await cryptocompare.price("XMR", currency);

        const conversionRate = data[currency];
        const convertedAmount = amount * conversionRate;
        converted.push(`${convertedAmount.toFixed(2)}`);  
      };

      const convertEmbed = new EmbedBuilder()
        .setColor("#ff6600")
        .setTitle(`${amount} XMR is equivalent to:`)
        .setURL("https://www.cryptocompare.com/coins/xmr/overview")
        .setDescription("Data provided by CryptoCompare")
        .setThumbnail("https://web.getmonero.org/press-kit/symbols/monero-symbol-480.png")
        .addFields(
          { name: ":flag_us: USD", value: `$${converted[0]}`, inline: true },
          { name: ":flag_eu: EUR", value: `€${converted[1]}`, inline: true },
          { name: ":flag_ca: CAD", value: `$${converted[2]}`, inline: true },
          { name: ":flag_gb: GBP", value: `£${converted[3]}`, inline: true },
          { name: ":flag_au: AUD", value: `$${converted[4]}`, inline: true },
          { name: ":flag_ch: CHF", value: `${converted[5]} CHF`, inline: true },
          { name: ":flag_jp: JPY", value: `¥${Math.floor(converted[6])}`, inline: true },
          { name: ":flag_cn: CNY", value: `¥${converted[7]}`, inline: true },
          { name: ":flag_in: INR", value: `₹${Math.floor(converted[8])}`, inline: true })
        .setTimestamp()

      await interaction.reply({ embeds: [convertEmbed] });
    } catch (error) {
      interaction.reply(`Error: ${error.message}`);
    }
  },
};

