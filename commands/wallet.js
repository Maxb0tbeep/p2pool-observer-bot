const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const axios = require('axios'); 

var api_shares;
var api_payouts;
var api_pool_info;

module.exports = {
	data: new SlashCommandBuilder()
		.setName('wallet')
		.setDescription('stats for a specific user on P2Pool')
    .addStringOption(pool =>
      pool.setName("pool")
        .setDescription("Which pool to check")
        .setRequired(true)
        .addChoices(
          { name: "main", value: "main" },
          { name: "mini", value: "mini" },
        ))
    .addStringOption( wallet =>
      wallet.setName("wallet")
        .setDescription("What wallet address or ID to check")
        .setRequired(true)
    ),
	async execute(interaction) {
    const pool = interaction.options.getString("pool");
    const wallet = interaction.options.getString("wallet");
    const oneDayAgo = Math.floor((Date.now() - 24 * 60 * 60 * 1000) / 1000);

    if(pool == "main") {
      api_shares = `https://p2pool.observer/api/shares?miner=${wallet}`;
      api_payouts = `https://p2pool.observer/api/payouts/${wallet}?search_limit=3`;
      api_pool_info = "https://p2pool.observer/api/pool_info";
    } else {
      api_shares = `https://mini.p2pool.observer/api/shares?miner=${wallet}`;
      api_payouts = `https://mini.p2pool.observer/api/payouts/${wallet}?search_limit=3`;
      api_pool_info = "https://mini.p2pool.observer/api/pool_info";
    }
    
    //i'm sorry for this
    axios.get(api_shares)
      .then(shareResponse => {
        axios.get(api_pool_info)
          .then(poolInfoResponse => {
            axios.get(api_payouts)
              .then(payoutResponse => {
                const windowShares = shareResponse.data.filter(share => poolInfoResponse.data.sidechain.last_block.side_height - share.side_height < shareResponse.data[0].window_depth); // comically long variable declaration
                const dayShares = shareResponse.data.filter(share => share.timestamp > oneDayAgo);

                const last_share = shareResponse.data[0].timestamp; 
                const pool_share_percent = null;
                const day_share_percent = null;
                const current_shares = windowShares.length;
                const estimated_hashrate = shareResponse.data[0].difficulty;
                const estimated_window_reward = null; 
                const day_shares = dayShares.length;
                const day_hashrate = null;
                const estimated_day_reward = null;

                try {
                  var payout1_amount = payoutResponse.data[0].coinbase_reward / 1e12;
                  payout1_amount = payout1_amount.toFixed(7);
                  var payout2_amount = payoutResponse.data[1].coinbase_reward / 1e12;
                  payout2_amount = payout2_amount.toFixed(7);
                  var payout3_amount = payoutResponse.data[2].coinbase_reward / 1e12;
                  payout3_amount = payout3_amount.toFixed(7);
                  const payout1_timestamp = payoutResponse.data[0].timestamp;
                  const payout2_timestamp = payoutResponse.data[1].timestamp;
                  const payout3_timestamp = payoutResponse.data[2].timestamp;

                  const p2poolEmbed = new EmbedBuilder()
                    .setColor("#ff6600")
                    .setTitle(`P2Pool ${pool} stats for ${wallet}`)
                    .setDescription("Data provided by https://p2pool.observer (may not be exact!)")
                    .setThumbnail("https://web.getmonero.org/press-kit/symbols/monero-symbol-480.png")
                    .addFields(
                      { name: "Last Share was", value: `<t:${last_share}:R>`, inline: true },
                      { name: "Pool Share", value: `${pool_share_percent}%`, inline: true },
                      { name: "Day Share", value: `${day_share_percent}%`, inline: true },
                      { name: "Window Shares", value: `${current_shares}`, inline: true },
                      { name: "Window Hashrate", value: `${estimated_hashrate} KH/s`, inline: true },
                      { name: "Window Reward", value: `${estimated_window_reward}`, inline: true },
                      { name: "Day Shares", value: `${day_shares}`, inline: true },
                      { name: "Day Hashrate", value: `${day_hashrate} KH/s`, inline: true },
                      { name: "Day Reward", value: `${estimated_day_reward}`, inline: true },
                      { name: "Last Payout #1", value: `${payout1_amount} XMR\n<t:${payout1_timestamp}:R>`, inline: true },
                      { name: "Last Payout #2", value: `${payout2_amount} XMR\n<t:${payout2_timestamp}:R>`, inline: true },
                      { name: "Last Payout #3", value: `${payout3_amount} XMR\n<t:${payout3_timestamp}:R>`, inline: true })
                    .setTimestamp()

                  interaction.reply({ embeds:[p2poolEmbed] }) 
                } catch (err) {
                  interaction.reply("Invalid wallet or does not have at least 3 payouts!");
                };
              }).catch(error => {
                interaction.reply(`Error: ${error}`)
              })
          }).catch(error => {
            interaction.reply(`Error: ${error}`)
          })
      }).catch(error => {
        interaction.reply(`Error: ${error}`)
      })
	},
};
